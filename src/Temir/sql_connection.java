package Temir;

import java.sql.*;
import javax.swing.JOptionPane;

public class sql_connection {

    Connection cin = null;
    PreparedStatement ps;
    static Statement st;
    int userid;

    public static Connection connectDB() {
        try {
            Class.forName("org.sqlite.JDBC");
            Connection con = DriverManager.getConnection("jdbc:sqlite:Temir.db");
            String tableProducts = "CREATE TABLE IF NOT EXISTS Products (\n"
                    + "	ID INTEGER PRIMARY KEY AUTOINCREMENT,\n"
                    + "	Name	TEXT,\n"
                    + "	Type	TEXT,\n"
                    + "	Problem TEXT,\n"
                    + "	Datetake TEXT,\n"
                    + "	Dategive TEXT,\n"
                    + "	SName	TEXT,\n"
                    + "	Number INTEGER,\n"
                    + "	Price	INTEGER,\n"
                    + "userid INTEGER)";
            String tableArxiv = "CREATE TABLE IF NOT EXISTS Arxiv (\n"
                    + "	ID     INTEGER,\n"
                    + "	Name	TEXT,\n"
                    + "	Type	TEXT,\n"
                    + "	Problem TEXT,\n"
                    + "	Datetake TEXT,\n"
                    + "	Dategive TEXT,\n"
                    + "	SName	TEXT,\n"
                    + "	Number INTEGER,\n"
                    + "	Price	INTEGER,\n"
                    + "userid INTEGER)";
            String usertable = "CREATE TABLE IF NOT EXISTS User (\n"
                    + "userid INTEGER PRIMARY KEY AUTOINCREMENT,\n"
                    + "	FirstName     TEXT,\n"
                    + "	LastName	TEXT,\n"
                    + "	Username	TEXT,\n"
                    + "	Password        TEXT )";
                   

            st = con.createStatement();
            st.execute(tableProducts);
            st.execute(tableArxiv);
            st.execute(usertable);
            st.close();

            // JOptionPane.showMessageDialog(null,"Connection established!");
            return con;
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
            return null;
        }
    }

}
