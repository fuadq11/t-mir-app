package Temir;

import java.util.Date;
/**
 *
 * @author Fuad
 */
import java.util.ArrayList;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Mehsullar {

    Connection con;
    Statement st;

    ResultSet rs;

    PreparedStatement pst = null;
//    String driver = "org.sqlite.JDBC";
//    String url = "jdbc:sqlite:C:\\Users\\Fuad.fuad\\Documents\\NetBeansProjects\\Temirdekiler2\\src\\Temir\\Temir.db";

    public ArrayList<String> prodct = new ArrayList();
    ArrayList<String> Arxivlist = new ArrayList();

    private String ad;
    private String nov;
    private Scanner x;
    private String Alinma;
    private String problem;
    private String sadi;
    private int elaqe;
    private int qiymet;
    private String Verilme;
    private static int userid;

    private int ID = 0;
    private String update;

    public int a = 0;
    private String directory = "C:/Temir/Fayllar";
    private File dir = new File(directory);

    public void setarxiv(int ID) throws SQLException {
        String query1 = "Select*From Products Where ID=?";
        pst = con.prepareStatement(query1);
        pst.setInt(1, ID);
        rs = pst.executeQuery();

        query1 = "Insert into Arxiv Values(?,?,?,?,?,?,?,?,?,?)";

        pst = con.prepareStatement(query1);

        pst.setInt(1, rs.getInt("ID"));
        pst.setString(2, rs.getString("Name"));
        pst.setString(3, rs.getString("Type"));
        pst.setString(4, rs.getString("Problem"));
        pst.setString(5, rs.getString("Datetake"));
        pst.setString(6, rs.getString("Dategive"));
        pst.setString(7, rs.getString("SName"));
        pst.setInt(8, rs.getInt("Number"));
        pst.setInt(9, rs.getInt("Price"));
        pst.setInt(10, rs.getInt("userid"));
        pst.execute();
        query1 = "Delete From Products Where ID=?";
        pst = con.prepareStatement(query1);
        pst.setInt(1, ID);
        pst.execute();
        pst.close();
    }

    /// mehsullar text fayldan oxunur 
    public void setPreviousProduct() {
        prodct.clear();
        try {
            dir.mkdirs();
            File file = new File(directory, "Products.txt");

            x = new Scanner(file);

            while (x.hasNextLine()) {
                prodct.add(x.nextLine());

            }
        } catch (FileNotFoundException ex) {

        }

    }

//Mehsullar text fayla yazilir
    public void recordFile() {

        try {
            dir.mkdirs();
            File file = new File(directory, "Products.txt");

            if (!file.exists()) {
                file.createNewFile();

            }
            try (PrintWriter pw = new PrintWriter(file)) {
                for (int i = 0; i < prodct.size(); i++) {
                    pw.println(prodct.get(i));

                }
                pw.close();
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Mehsullar.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Mehsullar.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
//Mehsullar ArrayListe yazilir

    public void setProduct() throws SQLException {

        con = sql_connection.connectDB();
        String query1 = "Insert into Products Values(?,?,?,?,?,?,?,?,?,?)";

        pst = con.prepareStatement(query1);
        pst.setString(2, ad);
        pst.setString(3, nov);
        pst.setString(4, problem);
        pst.setString(5, Alinma);
        pst.setString(6, Verilme);
        pst.setString(7, sadi);
        pst.setInt(8, elaqe);
        pst.setInt(9, qiymet);
        pst.setInt(10,userid);
        pst.execute();
        pst.close();

        /*
        if (prodct.size() != 0) {
            prodct.add(prodct.get(prodct.size() - 1));
            for (int i = prodct.size() - 1; i > 0; i--) {
                prodct.set(i, prodct.get(i - 1));

            }

            prodct.set(0, (this.ID + "#" + this.ad + "#" + this.nov + "#"
                    + this.problem + "#"
                    + this.verilme + "#" + this.sadi + "#" + this.elaqe + "#" + this.qiymet));

        } else {
            prodct.add(this.ID + "#" + this.ad + "#" + this.nov + "#"
                    + this.problem + "#"
                    + this.verilme + "#" + this.sadi + "#" + this.elaqe + "#" + this.qiymet);
        }
         */
    }

    /**
     *
     * @return name of product
     */
    public String getAd() {
        return ad;
    }

    public void setAd(String name) {
        this.ad = name;
    }

    /**
     *
     * @return
     */
    public String getNov() {
        return nov;
    }

    public void setNov(String type) {
        this.nov = type;
    }

    /**
     *
     * @return
     */
    public String getproblem() {
        return problem;
    }

    public void setProblem(String problem) {
        this.problem = problem;
    }

    /**
     *
     * @return Production date of product
     */
    public String getAlinma() {
        return Alinma;
    }

    public void setAlinma(String Alinma) {
        this.Alinma = Alinma;
    }

    public void setSadi(String sadi) {
        this.sadi = sadi;
    }

    public String getSadi() {
        return sadi;
    }

    public void setElaqe(int elaqe) {
        this.elaqe = elaqe;
    }

    public void setQiymet(int qiymet) {
        this.qiymet = qiymet;
    }

    public int getID() {
        return ID;

    }

    public void setverilme(String verilme) {
        this.Verilme = verilme;
    }
    public void setuserid(int id){
        this.userid=id;
    }
    public int getuserid(){
        return userid;
    }
    
    public void sqlconnection() throws SQLException {
        con = sql_connection.connectDB();

    }

}
